package tracker.hajj.android.hajjtracker;

import java.io.Serializable;

public class User implements Serializable {
    private  int id;
    private String F_name,L_name,address,city,country,nationality,B_day;

    public void setB_day(String b_day) {
        B_day = b_day;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setF_name(String f_name) {
        F_name = f_name;
    }

    public void setL_name(String l_name) {
        L_name = l_name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public int getId() {
        return id;
    }

    public String getF_name() {
        return F_name;
    }

    public String getL_name() {
        return L_name;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getNationality() {
        return nationality;
    }

    public String getB_day() {
        return B_day;
    }
}
