package tracker.hajj.android.hajjtracker;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class login extends AppCompatActivity {

    private EditText email, password;
    private ProgressDialog pDialog;
    private SessionManager session;
    private SQLiteHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Button login = (Button) findViewById(R.id.btnSignIn);
        Button register = (Button) findViewById(R.id.Register);
        email = (EditText) findViewById(R.id.email_login);
        password = (EditText) findViewById(R.id.password_login);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        db = new SQLiteHandler(getApplicationContext());
        // Session manager
        session = new SessionManager(getApplicationContext());

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {  
               /* String Semail = email.getText().toString().trim();
                String Spassword = password.getText().toString().trim();

                if (!Semail.isEmpty() && !Spassword.isEmpty()) {
                    // login user
                    checkLogin(Semail, Spassword);
                }else {
                        // Prompt user to enter credentials
                        Toast.makeText(getApplicationContext(),
                                "SVP vérifier vos coordonnées!", Toast.LENGTH_LONG)
                                .show();
                    } */
                Intent i = new Intent(login.this,mapping.class);
               // i.putExtra("email",email.getText().toString());
                startActivity(i);
            }
        });
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(login.this,register.class);
                startActivity(I);
            }
        });
    }

}

    /*private void checkLogin(String email, String password) {
        pDialog.setMessage("Se connecter...");
        showDialog();



    }*/
