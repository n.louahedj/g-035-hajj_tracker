FROM tiangolo/uwsgi-nginx-flask:python3.6


COPY ./app /app
COPY ./requirement.txt /tmp/requirement.txt
RUN pip install -r /tmp/requirement.txt
#VOLUME ["/app"]