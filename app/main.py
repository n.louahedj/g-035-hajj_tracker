# -*- coding: utf-8 -*-
"""

author : Noureddine Louahedj <n.louahedj@gmail.com>

"""

import os
from flask import Flask, abort, request, jsonify, g, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_httpauth import HTTPBasicAuth
from passlib.apps import custom_app_context as pwd_context
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)
from functools import wraps
import random
import string
from datetime import datetime

# initialization
app = Flask(__name__)
app.config['SECRET_KEY'] = 'the damp Hajj hackathon that we are not going to win ;p'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////app/data.db'
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# extensions
db = SQLAlchemy(app)
auth = HTTPBasicAuth()


## Country Model

class Country(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    country_code = db.Column(db.String(5))
    country_name = db.Column(db.String(30))

## Groups Table

groups = db.Table('groups',
    db.Column('group_id', db.Integer, db.ForeignKey('group.id'), primary_key=True),
    db.Column('user_id', db.Integer, db.ForeignKey('user.id'), primary_key=True),
    ) 


## Group Model

class Group(db.Model):
    id = db.Column(db.String(6), unique=True, primary_key=True)
    password = db.Column(db.String(6), nullable=False, primary_key=True)
    leader_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    start_date = db.Column(db.DateTime, nullable=False)
    end_date = db.Column(db.DateTime, nullable=False)
    users = db.relationship('User', secondary=groups, lazy='subquery',
        backref=db.backref('groups', lazy=True))


## User Model

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    email = db.Column(db.String(32), index=True, nullable=False)
    password_hash = db.Column(db.String(64), nullable=False)
    is_admin = db.Column(db.Boolean)

    f_name = db.Column(db.String(50), nullable=False)
    l_name = db.Column(db.String(50), nullable=False)
    
    address_line1 = db.Column(db.String(80))
    address_line2 = db.Column(db.String(80))
    city = db.Column(db.String(80))
    country_id = db.Column(db.Integer, db.ForeignKey('country.id'))
    zip_code = db.Column(db.String(10))
    nationality_id = db.Column(db.Integer)

    def hash_password(self, password):
        self.password_hash = pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)

    def generate_auth_token(self, expiration=600):
        s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
        return s.dumps({'id': self.id})

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None    # valid token, but expired
        except BadSignature:
            return None    # invalid token
        user = User.query.get(data['id'])
        return user

# Token required decoration
def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = request.args['token'] #http://127.0.0.1:5000/route?token=alshfjfjdklsfj89549834ur

        if not token:
            return jsonify({'message' : 'Token is missing!'}), 403

        if verify_password(token, None):
            return f(*args, **kwargs)
        else:
            return jsonify({'message' : 'Token is invalid!'}), 403

    return decorated

@auth.verify_password
def verify_password(email_or_token, password):
    # first try to authenticate by token
    user = User.verify_auth_token(email_or_token)
    if not user:
        # try to authenticate with email/password
        user = User.query.filter_by(email=email_or_token).first()
        if not user or not user.verify_password(password):
            return False
    g.user = user
    return True


# Routes
####
# Create new user
@app.route('/api/users', methods=['POST'])
def new_user():
    data = request.args
    email = data['email']
    password = data['password']
    f_name = data['f_name']
    l_name = data['l_name']

    if email is None or password is None or f_name is None or l_name is None:
        abort(400)    # missing arguments
    if User.query.filter_by(email=email).first() is not None:
        abort(400)    # existing user
    user = User(email=email, f_name=f_name, l_name=l_name, is_admin=False)
    user.hash_password(password)
    db.session.add(user)
    db.session.commit()
    return (
        jsonify({
            'email': user.email,
            'f_name': user.f_name,
            'l_name': user.l_name,
            }),
        201,
        {'Location': url_for('get_user', id=user.id, _external=True)},
        )

####
# Login
@app.route('/api/login', methods=['POST'])
def login():
    data = request.args
    email = data['email']
    password = data['password']
    if verify_password(email, password):
        token = g.user.generate_auth_token(60000)
        return jsonify({'token': token.decode('UTF-8'), 'duration': 60000})
    else:
        abort(400)

####
# Get user profile
@app.route('/api/users/<int:id>')
@token_required
def get_user(id):
    user = User.query.get(id)
    if not user:
        abort(400)
    return (jsonify({'email': user.email}), 201)

####
# Create group 
@app.route('/api/groups', methods=['POST'])
@token_required
def create_group():
    data = request.args
    start_date = datetime.strptime(data['start_date'], '%d-%m-%Y %H:%M:%S')
    end_date = datetime.strptime(data['end_date'], '%d-%m-%Y %H:%M:%S')
    if start_date is None or end_date is None:
        return (jsonify({
            'msg': 'An argument or more are missing',
            }),
            304,
            )
    leader_id = g.user.id
    password = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(6))
    id = ''.join(random.choice(string.digits) for _ in range(6))
    group = Group(id=id, password=password, start_date=start_date, end_date=end_date, leader_id=leader_id)
    db.session.add(group)
    db.session.commit()
    return (
        jsonify({
            'group': {
                'id': group.id,
                'password': group.password, 
                'start_date': group.start_date, 
                'end_date':group.end_date, 
                'leader_id':group.leader_id,
                }
            }), 
        201,
        )

# Create group 
@app.route('/api/groups/join', methods=['POST'])
@token_required
def join_group():
    data = request.args
    group_id = data['group_id']
    password = data['password']

    if group_id is None or password is None:
        return (jsonify({
            'msg': 'An argument or more are missing',
            }),
            304,
            )



@app.route('/api/resource')
@token_required
def get_resource():
    return jsonify({'data': 'Hello, %s!' % g.user.email})


if __name__ == '__main__':
    if not os.path.exists(app.config['SQLALCHEMY_DATABASE_URI']):
        db.create_all()
    app.run(debug=True)